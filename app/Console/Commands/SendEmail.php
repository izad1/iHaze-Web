<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $mandrill = new \Mandrill(env('MANDRILL_API_KEY'));

            $message = [     
                'text' => 'Lorem ipsum',
                'subject' => 'Test Email: '.date('Y-m-d H:i:s'),
                'from_email' => 'webmaster@elite.com.my',
                'from_name' => 'Elite Mailer',
                'to' => [
                    [
                        'email' => 'izadcm@gmail.com',
                        'name' => 'Izad Che Muda',
                        'type' => 'to'
                    ]
                ]
            ];
            
            $result = $mandrill->messages->send($message);                
        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    }
}
