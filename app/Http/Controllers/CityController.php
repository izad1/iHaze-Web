<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use Illuminate\Http\JsonResponse;
use App\Http\Requests;
use App\Http\Requests\ListCitiesRequest;
use App\Http\Controllers\Controller;
use Validator;
use Cache;


class CityController extends Controller {
    private $coords = [
        'Kota Tinggi' => [1.7293750, 103.8992270],
        'Larkin Lama' => [1.5297669, 103.6851294],
        'Muar' => [2.0630519, 102.5848717],
        'Pasir Gudang' => [1.4702880, 103.9029689],
        'Alor Setar' => [6.1248000, 100.3678178],
        'Bakar Arang, Sg. Petani' => [5.6279953, 100.4776837],
        'Langkawi' => [6.3500000, 99.8000000],
        'Tanah Merah' => [5.8088870, 102.1470772],
        'Bandaraya Melaka' => [2.1895940, 102.2500868],
        'Bukit Rambai' => [2.2530034, 102.1890042],
        'Nilai' => [2.8024550, 101.7988711],
        'Port Dickson' => [2.5225400, 101.7962932],
        'Seremban' => [2.7258890, 101.9378239],
        'Balok Baru, Kuantan' => [3.9381082, 103.3615610],
        'Indera Mahkota, Kuantan' => [3.8207630, 103.2969730],
        'Jerantut' => [3.9373950, 102.3620378],
        'Jalan Tasek, Ipoh' => [4.6502450, 101.1085770],
        'Kg. Air Putih, Taiping' => [4.8813480, 100.6823660],
        'S K Jalan Pegoh, Ipoh' => [4.5560358, 101.0805816],
        'Seri Manjung' => [4.1958680, 100.6647880],
        'Tanjung Malim' => [3.7058415, 101.5049163],
        'Kangar' => [6.4406330, 100.1983709],
        'Perai' => [5.3772940, 100.4001458],
        'Seberang Jaya 2, Perai' => [5.3818942, 100.3989242],
        'USM' => [5.355866, 100.302545],
        'Keningau' => [5.3374039, 116.1566801],
        'Kota Kinabalu' => [5.9804080, 116.0734568],
        'Sandakan' => [5.8394440, 118.1171729],
        'Tawau' => [4.2446510, 117.8911861],
        'Bintulu' => [3.1713220, 113.0419069],
        'Kapit' => [1.9951150, 112.9330850],
        'Kuching' => [1.6076812, 110.3785438],
        'Limbang' => [4.7550320, 115.0081459],
        'Miri' => [4.3994930, 113.9913832],
        'Samarahan' => [1.4427573, 110.4977108],
        'Sarikei' => [2.1317030, 111.5237279],
        'Sibu' => [2.2872840, 111.8305350],
        'Sri Aman' => [1.2370310, 111.4620790],
        'Banting' => [2.8120980, 101.5025551],
        'Kuala Selangor' => [3.3401840, 101.2497620],
        'Pelabuhan Kelang' => [2.9998520, 101.3928300],
        'Petaling Jaya' => [3.1278871, 101.5944885],
        'Shah Alam' => [3.0732810, 101.5184610],
        'Kemaman' => [4.2338358, 103.3638015],
        'Kuala Terengganu' => [5.3296240, 103.1370142],
        'Paka' => [4.6265810, 103.4399588],
        'Batu Muda,Kuala Lumpur' => [3.2278747, 101.6844759],
        'Cheras,Kuala Lumpur' => [3.1068447, 101.7259197],
        'Labuan' => [5.2831456, 115.2308250],
        'Putrajaya' => [2.9263610, 101.6964450],
        'SMK Tanjung Chat, Kota Bharu' => [6.148506, 102.239708],
        'ILP Miri' => [4.494576, 114.042438]
    ];    

    
    public function index(ListCitiesRequest $request) {   
        if (Cache::has($request->input('date'))) {
            return Cache::get($request->input('date'));
        }

        $client = new Client;        

        $cities = [];
        $times = [];    
        $crawler = $client->request('GET', 'http://apims.doe.gov.my/v2/hour1_'.$request->input('date').'.html');

        if (!$crawler->filter('#content table tr')->count()) {
            return [];
        }
           
        $crawler->filter('#content table tr')->each(function($node, $idx) use (&$cities, &$times) {
            $children = $node->children();

            if ($idx == 0) {
                for ($i = 2; $i <= 7; $i++) {               
                    $times[] = strip_tags(explode('<br>', $children->eq($i)->html())[1]);
                }
            }
            else {          
                $name = trim($children->eq(1)->text());

                $city = [
                    'name'  => $name,
                    'state' => $children->eq(0)->text()                    
                ];                

                if (isset($this->coords[$name])) {
                    $city['lat'] = $this->coords[$name][0];
                    $city['lng'] = $this->coords[$name][1];
                }
                else {
                    $city['lat'] = null;
                    $city['lng'] = null;
                }

                for ($i = 2; $i <= 7; $i++) {                                        
                    if(filter_var($children->eq($i)->text(), FILTER_SANITIZE_NUMBER_INT) == false) {
                        $city['readings'][$times[$i - 2]] = null;
                    }   
                    else {
                        $city['readings'][$times[$i - 2]] = $children->eq($i)->text();
                    }  
                }    

                $cities[] = $city;      
            }
        });

        for ($hour = 2; $hour <=4 ; $hour++) { 
            $times = [];    
            $crawler = $client->request('GET', 'http://apims.doe.gov.my/v2/hour'.$hour.'_'.$request->input('date').'.html');
               
            $crawler->filter('#content table tr')->each(function($node, $idx) use (&$cities, &$times) {
                $children = $node->children();

                if ($idx == 0) {
                    for ($i = 2; $i <= 7; $i++) {               
                        $times[] = strip_tags(explode('<br>', $children->eq($i)->html())[1]);
                    }
                }
                else {                          
                    for ($i = 2; $i <= 7; $i++) {                                        
                        if(filter_var($children->eq($i)->text(), FILTER_SANITIZE_NUMBER_INT) == false) {
                            $cities[$idx - 1]['readings'][$times[$i - 2]] = null;
                        }   
                        else {
                            $cities[$idx - 1]['readings'][$times[$i - 2]] = $children->eq($i)->text();
                        }  
                    }                        
                }
            });
        }      

        Cache::put($request->input('date'), $cities, 120);  

        return $cities;
    }
    
}
