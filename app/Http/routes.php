<?php

use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


Route::resource('city', 'CityController');


Route::get('/', function() {	
	return parse_url(env('REDIS_URL'));	
});


Route::get('test', 'MainController@getTest');


Route::get('geocode', function() {
	$cities = [
		'Kota Tinggi',
		'Larkin Lama',
		'Muar',
		'Pasir Gudang',	
		'Alor Setar',
		'Bakar Arang, Sg. Petani',
		'Langkawi',
		'SMK Tanjung Chat, Kota Bharu',
		'Tanah Merah',
		'Bandaraya Melaka',
		'Bukit Rambai',
		'Nilai',
		'Port Dickson',
		'Seremban',
		'Balok Baru, Kuantan',
		'Indera Mahkota, Kuantan',
		'Jerantut',
		'Jalan Tasek, Ipoh',
		'Kg. Air Putih, Taiping',
		'S K Jalan Pegoh, Ipoh',
		'Seri Manjung',
		'Tanjung Malim',
		'Kangar',
		'Perai',
		'Seberang Jaya 2, Perai',
		'USM',
		'Keningau',
		'Kota Kinabalu',
		'Sandakan',
		'Tawau',
		'Bintulu',
		'ILP Miri',
		'Kapit',
		'Kuching',
		'Limbang',
		'Miri',
		'Samarahan',
		'Sarikei',
		'Sibu',
		'Sri Aman',
		'Banting',
		'Kuala Selangor',
		'Pelabuhan Kelang',
		'Petaling Jaya',
		'Shah Alam',
		'Kemaman',
		'Kuala Terengganu',
		'Paka',
		'Batu Muda,Kuala Lumpur',
		'Cheras,Kuala Lumpur',
		'Labuan',
		'Putrajaya'		
	];
		
	$client = new Client;
	$text = "";

	foreach ($cities as $city) {
		$crawler = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/xml?address='.urlencode($city).'&key=AIzaSyAcONsuor0agt2n3odVDoONS4MVduGvMYE');                
        
        if ($crawler->filter('status')->text() == 'OK') {                    
            $lat = $crawler->filter('location lat')->text();
            $lng = $crawler->filter('location lng')->text();
            $text .= "'$city' => [$lat, $lng],\n";
        }		
	}

	return $text;
});