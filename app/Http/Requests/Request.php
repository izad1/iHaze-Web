<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;


abstract class Request extends FormRequest {
    
	protected function formatErrors(Validator $validator) {
        return ['errors' => $validator->errors()->all()];
    }


    public function response(array $errors) {        
        return new JsonResponse($errors, 422);        
    }

}
