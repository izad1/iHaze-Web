<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ListCitiesRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'date' => 'required|date_format:Y-m-d',
            //'period' => 'required|integer|in:1,2,3,4'
        ];
    }
}
