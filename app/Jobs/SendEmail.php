<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Job implements SelfHandling, ShouldQueue {
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $mandrill = new \Mandrill(env('MANDRILL_API_KEY'));

            $message = [     
                'text' => 'Example text content',
                'subject' => 'example subject',
                'from_email' => 'webmaster@elite.com.my',
                'from_name' => 'Elite Mailer',
                'to' => [
                    [
                        'email' => 'izadcm@gmail.com',
                        'name' => 'Izad Che Muda',
                        'type' => 'to'
                    ]
                ]
            ];
            
            $result = $mandrill->messages->send($message);

            print_r($result);       
        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    }
}
